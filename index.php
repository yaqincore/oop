<?php
require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . "</br>"; // "shaun"
echo "Legs : " . $sheep->legs . "</br>"; // 4
echo "cool blooded : " . $sheep->cold_blooded . "</br>" . "</br>"; // "no"

$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "</br>";
echo "Legs : " . $kodok->legs . "</br>";
echo "cool blooded : " . $kodok->cold_blooded . "</br>";
echo "Jump : " . $kodok->jump() . "</br>" . "</br>";

$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "</br>";
echo "Legs : " . $sungokong->legs . "</br>";
echo "cool blooded : " . $sungokong->cold_blooded . "</br>";
echo "Yell : " . $sungokong->yell() . "</br>" . "</br>";
